package com.sg.ayesberger.vendingmachine.service;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.math.BigDecimal;
import java.util.List;

public interface VendingMachineService {
    
    public List<Inventory> getAllAvailableItems() throws VendingMachineServiceException;
    
    BigDecimal getMoneyDeposited() throws VendingMachineServiceException;
    
    BigDecimal depositMoney(BigDecimal moneyToDeposit) throws VendingMachineServiceException;
    
    BigDecimal buyProduct(BigDecimal productCost, BigDecimal moneyDeposted) throws VendingMachineServiceException;
    
    BigDecimal getItemPrice(Integer itemChoice) throws VendingMachineServiceException;
    
    Inventory updateInventory(int itemChoice) throws VendingMachineServiceException;

    public void returnChange(BigDecimal change);
}
