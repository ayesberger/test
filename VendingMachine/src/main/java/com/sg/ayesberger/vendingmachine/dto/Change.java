package com.sg.ayesberger.vendingmachine.dto;

public class Change {

    public static int dollars = 0;
    public static int quarters = 0;
    public static int dimes = 0;
    public static int nickels = 0;
    public static int pennies = 0;

    public static void makeChange(int change) {

        dollars = change / 100;
        change = change % 100;

        quarters = change / 25;
        change = change % 25;

        dimes = change / 10;
        change = change % 10;

        nickels = change / 5;
        change = change % 5;

        pennies = change;

        System.out.println("Your change is: ");
        System.out.println("Dollars: " + dollars);
        System.out.println("Quarters: " + quarters);
        System.out.println("Dimes: " + dimes);
        System.out.println("Nickels: " + nickels);
        System.out.println("Pennies: " + pennies);

    }

}
