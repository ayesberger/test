package com.sg.ayesberger.vendingmachine.dao;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.math.BigDecimal;
import java.util.List;

public interface InventoryDAO {
    
    List<Inventory> displayInventory() throws InventoryDAOException;
    
    public void updateInventory(Integer productID, Inventory updatedInventory) throws InventoryDAOException;

    public Inventory addInventory(Integer productID, Inventory newInventory) throws InventoryDAOException;
    
    public Inventory removeInventory(Integer itemId) throws InventoryDAOException;
    
    public Inventory getItem(Integer productId) throws InventoryDAOException;
    
    public BigDecimal getItemPrice(Integer itemChoice) throws InventoryDAOException;
}
