package com.sg.ayesberger.vendingmachine.dao;

public class InventoryDAOException extends Exception {

    public InventoryDAOException() {
    }

    public InventoryDAOException(String msg) {
        super(msg);
    }

    public InventoryDAOException(String message, Throwable cause) {
        super(message, cause);
    }

}
