package com.sg.ayesberger.vendingmachine.ui;

public enum MenuOption {

    ADD_MONEY("Add Money"),
    BUY_ITEM("Purchase an Item"),
    QUIT("Get Change and Quit");

    private final String menuText;

    private MenuOption(String menuText) {
        this.menuText = menuText;
    }

    public String getMenuText() {
        return menuText;
    }

}
