package com.sg.ayesberger.vendingmachine.ui;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;

public class UserIOConsoleImpl implements UserIO {

    private final PrintStream out;
    private final Scanner scanner;

    public UserIOConsoleImpl(PrintStream outputStream,
            Scanner scanner) {
        this.out = outputStream;
        this.scanner = scanner;
    }

    @Override
    public void print(String message,
            Object... args) {
        out.format(message, args);
    }

    @Override
    public String readString(String prompt,
            Object... args) {
        return readString(0, Integer.MAX_VALUE, prompt, args);
    }

    @Override
    public String readString(int minLength,
            int maxLength,
            String prompt,
            Object... args) {
        String rv;
        do {
            out.format(prompt, args);
            rv = scanner.nextLine();
            if (rv.length() >= minLength && rv.length() <= maxLength) {
                break;
            }
            out.format("You must enter at least %d and no more than %d characters for the input. Press <ENTER> to continue: ",
                    minLength,
                    maxLength);
            scanner.nextLine();
        } while (true);
        return rv;
    }

    @Override
    public boolean readBoolean(String prompt,
            Object... args) {
        return readBoolean("Y|YES", "N|NO", false, prompt, args);
    }

    @Override
    public boolean readBoolean(String yesRegex,
            String noRegex,
            boolean caseSensitive,
            String prompt,
            Object... args) {
        boolean rv;
        if (!caseSensitive) {
            yesRegex = "(?i)" + yesRegex;
            noRegex = "(?i)" + noRegex;
        }
        do {
            out.format(prompt, args);
            String tmp = scanner.nextLine();
            if (tmp.matches(yesRegex) || tmp.matches(noRegex)) {
                rv = tmp.matches(yesRegex);
                break;
            }
            out.format("You must enter '%s' or '%s' to indicate your answer. Press <ENTER> to continue: ",
                    yesRegex,
                    noRegex);
            scanner.nextLine();
        } while (true);
        return rv;
    }

    @Override
    public int readInt(String prompt,
            Object... args) {
        return readInt(Integer.MIN_VALUE, Integer.MAX_VALUE, prompt, args);
    }

    @Override
    public int readInt(int min,
            int max,
            String prompt,
            Object... args) {
        int rv;
        do {
            out.format(prompt, args);
            try {
                rv = Integer.parseInt(scanner.nextLine());
                if (rv < min || rv > max) {
                    throw new NumberFormatException(String.format("Value must be between %d and %d (inclusive)",
                            min,
                            max));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid integer - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

    @Override
    public int readInt(Set<Integer> validIntegers, String prompt, Object... args) {
        int rv;
        do {
            out.format(prompt, args);
            try {
                rv = Integer.parseInt(scanner.nextLine());
                if (!validIntegers.contains(rv)) {
                    throw new NumberFormatException(String.format("Number must be one of: %s",
                            Arrays.deepToString(validIntegers.toArray())));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid integer - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

    @Override
    public long readLong(String prompt,
            Object... args) {
        return readLong(Long.MIN_VALUE, Long.MAX_VALUE, prompt, args);
    }

    @Override
    public long readLong(long min,
            long max,
            String prompt,
            Object... args) {
        long rv;
        do {
            out.format(prompt, args);
            try {
                rv = Long.parseLong(scanner.nextLine());
                if (rv < min || rv > max) {
                    throw new NumberFormatException(String.format("Value must be between %d and %d (inclusive)",
                            min,
                            max));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid integer - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

    @Override
    public float readFloat(String prompt,
            Object... args) {
        return readFloat(Float.MIN_VALUE, Float.MAX_VALUE, prompt, args);
    }

    @Override
    public float readFloat(float min,
            float max,
            String prompt,
            Object... args) {
        float rv;
        do {
            out.format(prompt, args);
            try {
                rv = Float.parseFloat(scanner.nextLine());
                if (rv < min || rv > max) {
                    throw new NumberFormatException(String.format("Value must be between %f and %f (inclusive)",
                            min,
                            max));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid floating-point value - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

    @Override
    public double readDouble(String prompt,
            Object... args) {
        return readDouble(Double.MIN_VALUE, Double.MAX_VALUE, prompt, args);
    }

    @Override
    public double readDouble(double min,
            double max,
            String prompt,
            Object... args) {
        double rv;
        do {
            out.format(prompt, args);
            try {
                rv = Double.parseDouble(scanner.nextLine());
                if (rv < min || rv > max) {
                    throw new NumberFormatException(String.format("Value must be between %f and %f (inclusive)",
                            min,
                            max));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid floating-point value - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

    @Override
    public BigDecimal readBigDecimal(BigDecimal min, BigDecimal max, String prompt) {
        BigDecimal rv;
        do {
            out.format(prompt);
            try {
                rv = new BigDecimal(scanner.nextLine());
                if (rv.compareTo(min) < 0 || rv.compareTo(max) > 0) {
                    throw new NumberFormatException(String.format("Value must be between %.2f and %.2f (inclusive)",
                            min,
                            max));
                }
                break;
            } catch (NumberFormatException ex) {
                out.format("You must enter a valid Decimal value - %s.%nPress <ENTER> to continue: ",
                        ex.getMessage());
                scanner.nextLine();
            }
        } while (true);
        return rv;
    }

}
