package com.sg.ayesberger.vendingmachine.service;

import com.sg.ayesberger.vendingmachine.dao.InventoryDAO;
import com.sg.ayesberger.vendingmachine.dao.InventoryDAOException;
import com.sg.ayesberger.vendingmachine.dto.Inventory;
import com.sg.ayesberger.vendingmachine.dto.Change;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public class VendingMachineServiceImpl implements VendingMachineService {

    private final InventoryDAO inventoryDAO;
    private BigDecimal moneyOnDeposit = new BigDecimal("0.00");

    public VendingMachineServiceImpl(InventoryDAO inventoryDAO) {
        this.inventoryDAO = inventoryDAO;
    }

    @Override
    public List<Inventory> getAllAvailableItems() throws VendingMachineServiceException {
        try {
            List<Inventory> items = inventoryDAO.displayInventory();
            return items
                    .stream()
                    .filter(s -> s.getNumberRemaining() > 0)
                    .collect(Collectors.toList());
        } catch (InventoryDAOException ex) {
            throw new VendingMachineServiceException("Unable to retrieve list of available items", ex);
        }
    }

    @Override
    public BigDecimal getMoneyDeposited() throws VendingMachineServiceException {
        return this.moneyOnDeposit;
    }

    @Override
    public BigDecimal depositMoney(BigDecimal moneyToDeposit) throws VendingMachineServiceException {
        this.moneyOnDeposit = this.moneyOnDeposit.add(moneyToDeposit);
        return this.moneyOnDeposit;
    }

    @Override
    public BigDecimal buyProduct(BigDecimal productCost, BigDecimal moneyDeposited) throws VendingMachineServiceException {
        BigDecimal change = new BigDecimal("0.00");
        change = moneyDeposited.subtract(productCost);
        moneyOnDeposit = change;
        return change;
    }

    @Override
    public BigDecimal getItemPrice(Integer itemChoice) throws VendingMachineServiceException {
        try {
            return inventoryDAO.getItemPrice(itemChoice);
        } catch (InventoryDAOException ex) {
            throw new VendingMachineServiceException("Unable to retrieve item price", ex);
        }
    }

    @Override
    public Inventory updateInventory(int itemChoice) throws VendingMachineServiceException {
        try {
            Inventory itemToEdit = inventoryDAO.getItem(itemChoice);
            Inventory updatedInventory = new Inventory(itemChoice);
            updatedInventory.setProductName(itemToEdit.getProductName());
            updatedInventory.setPrice(itemToEdit.getPrice());
            int numberRem = itemToEdit.getNumberRemaining() - 1;
            updatedInventory.setNumberRemaining(numberRem);
            inventoryDAO.updateInventory(itemChoice, updatedInventory);

            return updatedInventory;

        } catch (InventoryDAOException ex) {
            throw new VendingMachineServiceException("Unable to retrieve item price", ex);
        }
    }

    @Override
    public void returnChange(BigDecimal change) {
        BigDecimal rounded = change.setScale(2, RoundingMode.CEILING);
        int intChange = rounded.movePointRight(2).intValueExact();
        Change.makeChange(intChange);
    }
}
