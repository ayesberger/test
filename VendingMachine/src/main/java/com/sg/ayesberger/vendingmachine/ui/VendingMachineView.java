package com.sg.ayesberger.vendingmachine.ui;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.math.BigDecimal;
import java.util.List;

public class VendingMachineView {

    private static final MenuOption[] menuOptions = MenuOption.values();
    private static final ItemMenuOption[] itemMenuOptions = ItemMenuOption.values();

    private final UserIO io;

    public VendingMachineView(UserIO io) {
        this.io = io;
    }

    public void displayMenu() {
        io.print("%n%n========== Main Menu ==========%n%n");
        for (int i = 0; i < menuOptions.length; i++) {
            io.print("%d. %s%n", i + 1, menuOptions[i].getMenuText());
        }
    }

    public MenuOption promptForMenuChoice() {
        io.print("%n%n-------------------------------%n%n");
        int choice = io.readInt(1,
                menuOptions.length,
                "Enter Menu Selection (1 to %d): ",
                menuOptions.length);
        return menuOptions[choice - 1];
    }

    public void displayError(String message) {
        io.print("%nERROR: %s%n", message);
        promptToContinue();
    }

    private void promptToContinue() {
        io.readString("Please hit enter to continue...");
        io.print("");
    }

    public void displayAvailableItems(List<Inventory> availableItems) {
        io.print("%n%n========== Product Menu ==========%n%n");
        for (Inventory currentItem : availableItems) {
            io.print("\n" + "Product Number: " + currentItem.getProductID() + "  "
                    + "Product: " + currentItem.getProductName() + "  "
                    + "Price: $" + currentItem.getPrice() + "  "
                    + "Number remaining: " + currentItem.getNumberRemaining() + "\n");
        }
    }

    public void displayCurrentMoneyOnDeposit(BigDecimal moneyOnDeposit) {
        io.print("\n" + "You have " + moneyOnDeposit + " remaining." + "\n");
    }

    public BigDecimal promptForMoneyToDeposit() {
        String prompt = "Please enter the amount of money to deposit from $1.00 to $10.00: ";
        return io.readBigDecimal(BigDecimal.ONE, BigDecimal.TEN, prompt);
    }

    public void displayNewMoneyOnDeposit(BigDecimal moneyOnDeposit) {
        io.print("\n" + "You now have " + moneyOnDeposit + " remaining.");
    }

    public ItemMenuOption promptForItemChoice() {
        io.print(
                "%n%n-------------------------------%n%n");
        int choice = io.readInt(1,
                itemMenuOptions.length,
                "Enter Menu Selection (1 to %d): ",
                itemMenuOptions.length);
        return itemMenuOptions[choice - 1];
    }

    public void displayAddMoneyBanner() {
        io.print("=== Deposit Money ===");
    }

    public void displayBuyItemBanner() {
        io.print("=== Buy an Item ===");
    }

    public boolean confirmQuit() {
        return io.readBoolean("Would you like to exit and get your change?");
    }

    public void returnChangeBanner() {
        io.print("=== Return Change ===");
    }

    public void displayDisplayAvailableItemsBanner() {
        io.print("\n" + "======== Available Items ========");
    }

    public void displayInsuficientFunds() {
        io.print("\n" + "Insufficient Funds. Please add money to continue.");
    }

    public void displayBuySuccess() {
        io.print("Enjoy your treat!");
    }

    public void displayItemChoice(int itemChoice) {
        io.print("You chose item number: " + itemChoice);
    }

}
