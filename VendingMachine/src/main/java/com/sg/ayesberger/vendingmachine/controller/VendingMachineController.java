package com.sg.ayesberger.vendingmachine.controller;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import com.sg.ayesberger.vendingmachine.service.VendingMachineService;
import com.sg.ayesberger.vendingmachine.service.VendingMachineServiceException;
import com.sg.ayesberger.vendingmachine.ui.VendingMachineView;
import java.math.BigDecimal;
import java.util.List;

public class VendingMachineController {

    private final VendingMachineView view;
    private final VendingMachineService service;

    public VendingMachineController(VendingMachineView view, VendingMachineService service) {
        this.view = view;
        this.service = service;
    }

    public void run() throws VendingMachineServiceException {
        BigDecimal change = new BigDecimal("0.00");
        BigDecimal moneyOnDeposit = service.getMoneyDeposited();
        do {
            view.displayDisplayAvailableItemsBanner();
            displayInventory();
            view.displayMenu();
            switch (view.promptForMenuChoice()) {
                case ADD_MONEY:
                    view.displayAddMoneyBanner();
                    depositMoney();
                    break;
                case BUY_ITEM:
                    if (moneyOnDeposit.compareTo(new BigDecimal("0")) <= 0) {
                        view.displayInsuficientFunds();
                    } else {
                        view.displayBuyItemBanner();
                        displayInventory();
                        view.promptForItemChoice();
                        int itemChoice = itemChoice();                       
                        BigDecimal itemPrice = getItemPrice(itemChoice);
                        moneyOnDeposit = service.getMoneyDeposited();
                        if (moneyOnDeposit.compareTo(itemPrice) <= 0) {
                            view.displayInsuficientFunds();
                            view.displayCurrentMoneyOnDeposit(moneyOnDeposit);
                        } else {
                            view.displayItemChoice(itemChoice);
                            change = buyItem(itemPrice, itemChoice);
                        }

                    }
                    break;
                case QUIT:
                    if (reallyQuit()) {
                        returnChange(change);
                        return;
                    }
                default:
                    view.displayError("Unhandled Menu Option");
            }
        } while (true);
    }

    private void depositMoney() throws VendingMachineServiceException {
        BigDecimal moneyOnDeposit = service.getMoneyDeposited();
        view.displayCurrentMoneyOnDeposit(moneyOnDeposit);
        BigDecimal moneyToDeposit = view.promptForMoneyToDeposit();
        moneyOnDeposit = service.depositMoney(moneyToDeposit);
        view.displayNewMoneyOnDeposit(moneyOnDeposit);
    }

    private int itemChoice() {
        int itemChoice = 0;
        switch (view.promptForItemChoice()) {

            case CHOCO_CHIP:
                itemChoice = 1;
                break;
            case SUGAR:
                itemChoice = 2;
                break;
            case BROWNIE:
                itemChoice = 3;
                break;
            case CARAMEL:
                itemChoice = 4;
                break;
            case PEPPERMINT:
                itemChoice = 5;
                break;
            case GUMDROP:
                itemChoice = 6;
                break;
            default:
                view.displayError("Unhandled Menu Option");
        }
        return itemChoice;
    }

    private BigDecimal buyItem(BigDecimal itemPrice, int itemChoice) throws VendingMachineServiceException {

        BigDecimal moneyDeposited = service.getMoneyDeposited();
        service.updateInventory(itemChoice);
        view.displayBuySuccess();
        return service.buyProduct(itemPrice, moneyDeposited);
    }

    private void displayInventory() {
        try {
            List<Inventory> availableItems = service.getAllAvailableItems();
            view.displayAvailableItems(availableItems);
        } catch (VendingMachineServiceException ex) {
            view.displayError(ex.getMessage());
        }
    }

    private BigDecimal getItemPrice(int itemChoice) throws VendingMachineServiceException {
        return service.getItemPrice(itemChoice);
    }

    private boolean reallyQuit() {
        return view.confirmQuit();
    }

    private void returnChange(BigDecimal change) {
        view.returnChangeBanner();
        service.returnChange(change);
    }

}
