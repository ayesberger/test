package com.sg.ayesberger.vendingmachine;

import com.sg.ayesberger.vendingmachine.controller.VendingMachineController;
import com.sg.ayesberger.vendingmachine.dao.InventoryDAO;
import com.sg.ayesberger.vendingmachine.dao.InventoryDAOFileImpl;
import com.sg.ayesberger.vendingmachine.service.VendingMachineService;
import com.sg.ayesberger.vendingmachine.service.VendingMachineServiceException;
import com.sg.ayesberger.vendingmachine.service.VendingMachineServiceImpl;
import com.sg.ayesberger.vendingmachine.ui.UserIO;
import com.sg.ayesberger.vendingmachine.ui.UserIOConsoleImpl;
import com.sg.ayesberger.vendingmachine.ui.VendingMachineView;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws VendingMachineServiceException {
        // View
        Scanner scanner = new Scanner(System.in);
        UserIO io = new UserIOConsoleImpl(System.out, scanner);
        VendingMachineView view = new VendingMachineView(io);

        // DAO
        InventoryDAO InventoryDao = new InventoryDAOFileImpl();

        // Service
        VendingMachineService service = new VendingMachineServiceImpl(InventoryDao);

        // Controller
        VendingMachineController controller = new VendingMachineController(view, service);

        controller.run();
    }

}
