package com.sg.ayesberger.vendingmachine.dao;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class InventoryDAOFileImpl implements InventoryDAO {

    public static final String INVENTORY_FILE = "inventory.txt";
    public static final String DELIMITER = "::";

    private Map<Integer, Inventory> inventory = new HashMap<>();

    @Override
    public Inventory addInventory(Integer productID, Inventory newInventory) throws InventoryDAOException {
        Inventory newProduct = inventory.put(productID, newInventory);
        writeInventory();
        return newProduct;
    }

    @Override
    public Inventory removeInventory(Integer itemId) throws InventoryDAOException {
        Inventory removedItem = inventory.remove(itemId);
        writeInventory();
        return removedItem;
    }

    @Override
    public List<Inventory> displayInventory() throws InventoryDAOException {
        loadInventory();
        return new ArrayList<Inventory>(inventory.values());
    }

    @Override
    public void updateInventory(Integer productID, Inventory updatedInventory) throws InventoryDAOException {
        inventory.put(productID, updatedInventory);
        writeInventory();
    }

    private void loadInventory() throws InventoryDAOException {

        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(INVENTORY_FILE)))) {

            String currentLine;
            String[] currentTokens;

            while (scanner.hasNextLine()) {
                currentLine = scanner.nextLine();
                currentTokens = currentLine.split(DELIMITER);
                Inventory currentProduct = new Inventory(Integer.parseInt(currentTokens[0]));
                currentProduct.setProductName(currentTokens[1]);
                currentProduct.setPrice(new BigDecimal(currentTokens[2]));
                currentProduct.setNumberRemaining(Integer.parseInt(currentTokens[3]));

                inventory.put(currentProduct.getProductID(), currentProduct);
            }

        } catch (FileNotFoundException e) {
            throw new InventoryDAOException("-_- Could not load inventory data into memory.", e);
        }
    }

    private void writeInventory() throws InventoryDAOException {
        try (PrintWriter out = new PrintWriter(new FileWriter(INVENTORY_FILE))) {
            List<Inventory> InventoryList = this.displayInventory();
            for (Inventory currentItem : InventoryList) {
                out.println(currentItem.getProductID() + DELIMITER
                        + currentItem.getProductName() + DELIMITER
                        + currentItem.getPrice() + DELIMITER
                        + currentItem.getNumberRemaining());

                out.flush();
            }

        } catch (IOException e) {
            throw new InventoryDAOException("Could not save inventory data.", e);
        }
    }

    @Override
    public BigDecimal getItemPrice(Integer itemChoice) throws InventoryDAOException {
        loadInventory();
        Inventory chosenItem = inventory.get(itemChoice);
        return chosenItem.getPrice();
    }

    @Override
    public Inventory getItem(Integer productId) throws InventoryDAOException {
        loadInventory();
        return inventory.get(productId);
    }
}
