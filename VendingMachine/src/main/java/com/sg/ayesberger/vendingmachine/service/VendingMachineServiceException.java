package com.sg.ayesberger.vendingmachine.service;

public class VendingMachineServiceException extends Exception {

    public VendingMachineServiceException(String message) {
        super(message);
    }

    public VendingMachineServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
