package com.sg.ayesberger.vendingmachine.dto;

import java.math.BigDecimal;
import java.util.Objects;

public class Inventory {

    private int productID;
    private String productName;
    private BigDecimal price;
    private int numberRemaining;

    public Inventory(int productID) {
        this.productID = productID;
    }

    public int getProductID() {
        return productID;
    }
    
    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getNumberRemaining() {
        return numberRemaining;
    }

    public void setNumberRemaining(int numberRemaining) {
        this.numberRemaining = numberRemaining;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.productName);
        hash = 47 * hash + Objects.hashCode(this.price);
        hash = 47 * hash + this.numberRemaining;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inventory other = (Inventory) obj;
        if (this.numberRemaining != other.numberRemaining) {
            return false;
        }
        if (!Objects.equals(this.productName, other.productName)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }

}
