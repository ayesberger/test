package com.sg.ayesberger.vendingmachine.ui;

public enum ItemMenuOption {
    CHOCO_CHIP("Chocolate Chip Cookie Bites"),
    SUGAR("Frosted Sugar Cookie"),
    BROWNIE("Mocha Brownie"),
    CARAMEL("Sea Salt Caramel"),
    PEPPERMINT("Peppermint Bark"),
    GUMDROP("Gum Drops");

    private final String itemMenuText;

    private ItemMenuOption(String menuText) {
        this.itemMenuText = menuText;
    }

    public String getMenuText() {
        return itemMenuText;
    }
    
    /*
    Sample Menu for text file:
1::Chocolate Chip Cookie Bites::3.25::10
2::Frosted Sugar Cookie::2.70::15
3::Mocha Brownie::3.99::12
4::Sea Salt Caramel::1.88::20
5::Peppermint Bark::2.11::20
6::Gum Drops::.45::0
    */
}
