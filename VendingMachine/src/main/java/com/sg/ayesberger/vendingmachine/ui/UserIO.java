package com.sg.ayesberger.vendingmachine.ui;

import java.math.BigDecimal;
import java.util.Set;

public interface UserIO {

    void print(String message, Object... args);

    String readString(String prompt, Object... args);

    String readString(int minLength, int maxLength, String prompt, Object... args);

    boolean readBoolean(String prompt, Object... args);

    boolean readBoolean(String yesRegex, String noRegex, boolean caseSensitive, String prompt, Object... args);

    int readInt(String prompt, Object... args);

    int readInt(int min, int max, String prompt, Object... args);

    int readInt(Set<Integer> validIntegers, String prompt, Object... args);

    long readLong(String prompt, Object... args);

    long readLong(long min, long max, String prompt, Object... args);

    float readFloat(String prompt, Object... args);

    float readFloat(float min, float max, String prompt, Object... args);

    double readDouble(String prompt, Object... args);

    double readDouble(double min, double max, String prompt, Object... args);

    BigDecimal readBigDecimal(BigDecimal min, BigDecimal max, String prompt);

}
