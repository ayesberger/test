package com.sg.ayesberger.vendingmachine.service;

import com.sg.ayesberger.vendingmachine.dao.InventoryDAO;
import com.sg.ayesberger.vendingmachine.dao.InventoryDAOException;
import com.sg.ayesberger.vendingmachine.dao.InventoryDAOFileImpl;
import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class VendingMachineServiceImplTest {

    private InventoryDAO dao = new InventoryDAOFileImpl();
    private VendingMachineService service = new VendingMachineServiceImpl(dao);

    public VendingMachineServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws InventoryDAOException {
        List<Inventory> inventoryList = dao.displayInventory();
        for (Inventory item : inventoryList) {
            dao.removeInventory(item.getProductID());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAllAvailableItems method, of class VendingMachineServiceImpl.
     */
    @Test
    public void testGetAllAvailableItems() throws Exception {
        Inventory item = new Inventory(Integer.parseInt("1"));
        item.setProductName("Brownie");
        item.setPrice(new BigDecimal("1.25"));
        item.setNumberRemaining(Integer.parseInt("10"));

        dao.addInventory(item.getProductID(), item);

        Inventory item2 = new Inventory(Integer.parseInt("2"));
        item2.setProductName("Cookies");
        item2.setPrice(new BigDecimal("1.75"));
        item2.setNumberRemaining(Integer.parseInt("0"));

        dao.addInventory(item2.getProductID(), item2);

        assertEquals(2, dao.displayInventory().size());
        assertEquals(1, service.getAllAvailableItems().size());
    }

    /**
     * Test of getMoneyDeposited method and depositMoney method, of class
     * VendingMachineServiceImpl.
     */
    @Test
    public void testsAddMoneyAndGetMoneyDeposited() throws Exception {
        service.depositMoney(new BigDecimal("10.50"));
        BigDecimal money = new BigDecimal("10.50");
        assertEquals(money, service.getMoneyDeposited());
    }

    /**
     * Test of buyProduct method, of class VendingMachineServiceImpl.
     */
    @Test
    public void testBuyProduct() throws Exception {
    }

    /**
     * Test of getItemPrice method, of class VendingMachineServiceImpl.
     */
    @Test
    public void testGetItemPrice() throws Exception {
    }

    /**
     * Test of updateInventory method, of class VendingMachineServiceImpl.
     */
    @Test
    public void testUpdateInventory() throws Exception {
        Inventory item = new Inventory(Integer.parseInt("1"));
        item.setProductName("Brownie");
        item.setPrice(new BigDecimal("1.25"));
        item.setNumberRemaining(Integer.parseInt("10"));

        dao.addInventory(item.getProductID(), item);
        Inventory updatedInventory = service.updateInventory(1);
        
        assertEquals(9, updatedInventory.getNumberRemaining());
    }

}
