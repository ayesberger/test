package com.sg.ayesberger.vendingmachine.dao;

import com.sg.ayesberger.vendingmachine.dto.Inventory;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class InventoryDAOFileImplTest {

    private InventoryDAO dao = new InventoryDAOFileImpl();

    public InventoryDAOFileImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws InventoryDAOException {
        List<Inventory> inventoryList = dao.displayInventory();
        for (Inventory item : inventoryList) {
            dao.removeInventory(item.getProductID());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addInventory method, of class InventoryDAOFileImpl.
     */
    @Test
    public void testAddInventory() throws Exception {

        Inventory item = new Inventory(Integer.parseInt("1"));
        item.setProductName("Brownie");
        item.setPrice(new BigDecimal("1.25"));
        item.setNumberRemaining(Integer.parseInt("10"));

        dao.addInventory(item.getProductID(), item);

        Inventory fromDao = dao.getItem(item.getProductID());

        assertEquals(item, fromDao);
    }

    /**
     * Test of displayInventory method, of class InventoryDAOFileImpl.
     */
    @Test
    public void testDisplayInventory() throws Exception {
        Inventory item = new Inventory(Integer.parseInt("1"));
        item.setProductName("Brownie");
        item.setPrice(new BigDecimal("1.25"));
        item.setNumberRemaining(Integer.parseInt("10"));

        dao.addInventory(item.getProductID(), item);

        Inventory item2 = new Inventory(Integer.parseInt("2"));
        item2.setProductName("Cookies");
        item2.setPrice(new BigDecimal("1.75"));
        item2.setNumberRemaining(Integer.parseInt("8"));

        dao.addInventory(item2.getProductID(), item2);

        assertEquals(2, dao.displayInventory().size());
    }

    /**
     * Test of updateInventory method, of class InventoryDAOFileImpl.
     */
    @Test
    public void testUpdateInventory() throws Exception {

        Inventory item = new Inventory(Integer.parseInt("1"));
        item.setProductName("Brownie");
        item.setPrice(new BigDecimal("1.25"));
        item.setNumberRemaining(Integer.parseInt("10"));

        dao.addInventory(item.getProductID(), item);

        Inventory updatedInventory = new Inventory(Integer.parseInt("1"));
        updatedInventory.setProductName(item.getProductName());
        updatedInventory.setPrice(item.getPrice());
        int numberRem = item.getNumberRemaining() - 1;
        updatedInventory.setNumberRemaining(numberRem);

        dao.updateInventory(1, updatedInventory);
        assertEquals(9, updatedInventory.getNumberRemaining());

    }

    /**
     * Test of getItemPrice method, of class InventoryDAOFileImpl.
     */
    @Test
    public void testGetItemPrice() throws Exception {

    }

}
