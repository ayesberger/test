package com.sg.ayesberger.studentquizscores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class App {

    public static UserInterface myUserInterface = new UserInterface();
    public static ArrayList<Integer> scores = new ArrayList<>();
    public static Map<String, ArrayList<Integer>> studentScores = new HashMap<>();

    public static void main(String[] args) {

        promptForUserChoice();

    }

    public static void promptForUserChoice() {
        int userChoice = myUserInterface.readInt("1) View a list of students. 2) Add a student to the system. 3) Remove a student from the system. 4) View a student's quiz scores. 5) View the average quiz score for a student. 6) Exit.", 1, 6);

        switch (userChoice) {
            case 1:
                viewListOfStudents();
                break;
            case 2:
                addStudents();
                break;
            case 3:
                removeStudents();
                break;
            case 4:
                //view a student's quiz scores
                break;
            case 5:
                // view a student's average quiz score
                break;
            case 6:
                //quit program.
                break;
        }
    }
    
    public static void viewListOfStudents() {
        
        Set<String> namesOfStudents = studentScores.keySet();
        for (String k : namesOfStudents) {
            System.out.println(studentScores.get(k));
        }
    }
    
    public static void addStudents() {
        String newStudent = myUserInterface.readString("What is the name of the sudent you would like added to the system?");
        int studentScore = myUserInterface.readInt("What score would you like to add for " + newStudent + "?" );
        scores.add(studentScore);
        studentScores.put(newStudent, scores);
    }
    
    public static void removeStudents() {
        String removeMe = myUserInterface.readString("What student would you like to remove?");
        studentScores.remove(removeMe);
    }
}
