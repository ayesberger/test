package com.sg.ayesberger.studentquizscores;

import java.util.Scanner;

public class UserInterface implements UserIO {
    
    Scanner userInput = new Scanner(System.in);

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        String stringUserInput = userInput.nextLine();

        return stringUserInput;
    }
    
    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public int readInt(String prompt) {
        System.out.println(prompt);
        String stringUserInt = userInput.nextLine();
        int userInt = Integer.parseInt(stringUserInt);

        return userInt;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int userInt = 0;
        boolean validNumber = false;
        while (!validNumber) {
            System.out.println(prompt);
            String stringUserInt = userInput.nextLine();
            userInt = Integer.parseInt(stringUserInt);

            if (userInt >= min && userInt <= max) {
                validNumber = true;

            } else {
                System.out.println("Please enter a valid number.");
            }

        }
        return userInt;
    }
}

