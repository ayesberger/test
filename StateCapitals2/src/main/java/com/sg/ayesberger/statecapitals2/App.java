package com.sg.ayesberger.statecapitals2;

import java.util.Map;

public class App {
    
    public static States stateInformation = new States();

    public static void main(String[] args) {

        for (Map.Entry<String, Capital> entry : stateInformation.states.entrySet()) {
            
            System.out.println(entry.getKey() + " " + entry.getValue());
            
            // need to print state name + capital + population + square mileage
            //System.out.println(state + capitalClass.getStateName() + capitalClass.getCapitalName() + capitalClass.getPopulation() + capitalClass.getSquareMileage());

        }

    }

}
