package com.sg.ayesberger.statecapitals2;

import java.util.HashMap;
import java.util.Map;

public class States {
    
        Map<String, Capital> states = new HashMap<>();      
// I used the 2010 census data found on Wikipedia.
    public States() {
        states.put("Alabama", new Capital("Montgomery", 205764, 156));
        states.put("Alaska", new Capital("Juneau", 31275, 2717));
        states.put("Arizona", new Capital("Pheonix", 1445632, 475));
        states.put("Arkansas", new Capital("Little Rock", 193524, 116));
        states.put("California", new Capital("Sacramento", 466488, 97));
        states.put("Colorado", new Capital("Denver", 600158, 153));
        states.put("Connecticut", new Capital("Hartford", 124775, 17));
        states.put("Delaware", new Capital("Dover", 36047, 22));
        states.put("Florida", new Capital("Tallahassee", 18412, 96));
        states.put("Georgia", new Capital("Atlanta", 420003, 96));
        states.put("Hawaii", new Capital("Honolulu", 337256, 86));
        states.put("Idaho", new Capital("Boise", 205671, 64));
        states.put("Illinois", new Capital("Springfield", 116250, 54));
        states.put("Indiana", new Capital("Indianapolis", 829718, 362));
        states.put("Iowa", new Capital("Des Moines", 203433, 76));
        states.put("Kansas", new Capital("Topeka", 127473, 56));
        states.put("Kentucky", new Capital("Frankfortt", 25527, 15));
        states.put("Louisiana", new Capital("Baton Rouge", 229553, 77));
        states.put("Maine", new Capital("Augusta", 19136, 55));
        states.put("Maryland", new Capital("Annapolis", 38394, 7));
        states.put("Massachusetts", new Capital("Boston", 617594, 48));
        states.put("Michigan", new Capital("Lansing", 114297, 35));
        states.put("Minnesota", new Capital("Saint Paul", 285068, 53));
        states.put("Mississippi", new Capital("Jackson", 173514, 105));
        states.put("Missouri", new Capital("Jefferson City", 43079, 27));
        states.put("Montana", new Capital("Helena", 28190, 14));
        states.put("Nebraska", new Capital("Lincoln", 258379, 75));
        states.put("Nevada", new Capital("Carson City", 55274, 143));
        states.put("New Hampshire", new Capital("Concord", 42695, 64));
        states.put("New Jersey", new Capital("Trenton", 84913, 8));
        states.put("New Mexico", new Capital("Santa Fe", 75764, 37));
        states.put("New York", new Capital("Albany", 97856, 21));
        states.put("North Carolina", new Capital("Raleigh", 403892, 115));
        states.put("North Dakota", new Capital("Bismarck", 61272, 27));
        states.put("Ohio", new Capital("Columbus", 822553, 210));
        states.put("Oklahoma", new Capital("Oklahoma City", 580000, 607));
        states.put("Oregon", new Capital("Salem", 154637, 46));
        states.put("Pennsylvania", new Capital("Harrisburg", 49528, 8));
        states.put("Rhode Island", new Capital("Providence", 178042, 19));
        states.put("South Carolina", new Capital("Columbia", 131686, 125));
        states.put("South Dakota", new Capital("Pierre", 13646, 13));
        states.put("Tennessee", new Capital("Nashville", 635710, 473));
        states.put("Texas", new Capital("Austin", 790390, 252));
        states.put("Utah", new Capital("Salt Lake City", 186440, 109));
        states.put("Vermont", new Capital("Montpelier", 7855, 10));
        states.put("Virginia", new Capital("Richmond", 204214, 60));
        states.put("Washington", new Capital("Olympia", 46478, 17));
        states.put("West Virginia", new Capital("Charleston", 51400, 32));
        states.put("Wisconsin", new Capital("Madison", 233209, 69));
        states.put("Wyoming", new Capital("Cheyenne", 59466, 21));
    }
}
