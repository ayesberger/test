package com.sg.ayesberger.statecapitals2;

import java.util.Set;

public class Capital {

    States stateInformation = new States();
    public Set<String> stateName = stateInformation.states.keySet();
    String capitalName;
    int population;
    int squareMileage;

    public Capital(String capitalName, int population, int squareMileage) {
        this.capitalName = capitalName;
        this.population = population;
        this.squareMileage = squareMileage;

    }

    public States getStateInformation() {
        return stateInformation;
    }

    public void setStateInformation(States stateInformation) {
        this.stateInformation = stateInformation;
    }

    public Set<String> getStateName() {
        return stateName;
    }

    public void setStateName(Set<String> stateName) {
        this.stateName = stateName;
    }

    public String getCapitalName() {
        return capitalName;
    }

    public void setCapitalName(String capitalName) {
        this.capitalName = capitalName;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getSquareMileage() {
        return squareMileage;
    }

    public void setSquareMileage(int squareMileage) {
        this.squareMileage = squareMileage;
    }

}
