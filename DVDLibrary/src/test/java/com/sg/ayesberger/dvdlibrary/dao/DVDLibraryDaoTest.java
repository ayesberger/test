package com.sg.ayesberger.dvdlibrary.dao;

import com.sg.ayesberger.dvdlibrary.dto.DVD;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DVDLibraryDaoTest {

    private DVDLibraryDao dao = new DVDLibraryDaoFileImpl();

    public DVDLibraryDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
        List<DVD> DVDList = dao.getAllDVDs();
        for (DVD dvd : DVDList) {
            dao.removeDVD(dvd.getDVDId());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addDVD method, of class DVDLibraryDao.
     */
    @Test
    public void testAddGetDVD() throws Exception {
        DVD dvd = new DVD("0001");
        dvd.setDVDTitle("Fish");
        dvd.setDirector("Frank");
        dvd.setMPAARating("G");
        dvd.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd.setStudio("Ghibli");
        dvd.setUserRating("10/10");

        dao.addDVD(dvd.getDVDId(), dvd);

        DVD fromDao = dao.getDVD(dvd.getDVDId());

        assertEquals(dvd, fromDao);
    }

    /**
     * Test of getAllDVDs method, of class DVDLibraryDao.
     */
    @Test
    public void testGetAllDVDs() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("01/22/2002", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        assertEquals(2, dao.getAllDVDs().size());
    }

    /**
     * Test of removeDVD method, of class DVDLibraryDao.
     */
    @Test
    public void testRemoveDVD() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/2000", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        dao.removeDVD(dvd1.getDVDId());
        assertEquals(1, dao.getAllDVDs().size());
        assertNull(dao.getDVD(dvd1.getDVDId()));

        dao.removeDVD(dvd2.getDVDId());
        assertEquals(0, dao.getAllDVDs().size());
        assertNull(dao.getDVD(dvd2.getDVDId()));
    }

    /**
     * Test of searchDVDs method, of class DVDLibraryDao.
     */
    @Test
    public void testSearchDVDsExpectDVD() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/2000", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD testDVD = dao.searchDVDs("Big Fish");
        assertEquals(testDVD, dvd1);
    }

    @Test
    public void testSearchDVDsExpectedNull() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/2000", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD testContact = dao.searchDVDs("Potato");
        assertNull(testContact);
    }

    /**
     * Test of editDVD method, of class DVDLibraryDao.
     */
    @Test
    public void testEditDVDChangingFields() throws Exception {
        DVD dvd = new DVD("0001");
        dvd.setDVDTitle("Fish");
        dvd.setDirector("Frank");
        dvd.setMPAARating("G");
        dvd.setReleaseDate(LocalDate.parse("01/02/2016", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd.setStudio("Ghibli");
        dvd.setUserRating("10/10");

        dao.addDVD(dvd.getDVDId(), dvd);

        DVD editedDVD = dvd;
        editedDVD.setDVDTitle("Cat");
        editedDVD.setDirector("Sam");
        editedDVD.setMPAARating("PG");
        editedDVD.setReleaseDate(LocalDate.parse("10/10/2014", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        editedDVD.setStudio("Miramax");
        editedDVD.setUserRating("9/10");

        DVD testDVD = dao.editDVD(dvd.getDVDId(), editedDVD);

        assertEquals(testDVD, editedDVD);

    }

    @Test
    public void testEditDVDNotChangingFields() throws Exception {
        DVD dvd = new DVD("0001");
        dvd.setDVDTitle("Fish");
        dvd.setDirector("Frank");
        dvd.setMPAARating("G");
        dvd.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd.setStudio("Ghibli");
        dvd.setUserRating("10/10");

        dao.addDVD(dvd.getDVDId(), dvd);

        DVD editedDVD = dvd;
        editedDVD.setDVDTitle("Fish");
        editedDVD.setDirector("Frank");
        editedDVD.setMPAARating("G");
        editedDVD.setReleaseDate(LocalDate.parse("02/01/2001", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        editedDVD.setStudio("Ghibli");
        editedDVD.setUserRating("10/10");

        DVD testDVD = dao.editDVD(dvd.getDVDId(), editedDVD);

        assertEquals(testDVD, editedDVD);

    }

    @Test
    public void testFindMoviesReleasedByNYears() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/1950", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/1960", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD dvd3 = new DVD("0003");
        dvd3.setDVDTitle("Fuzz");
        dvd3.setDirector("Bear");
        dvd3.setMPAARating("PG-13");
        dvd3.setReleaseDate(LocalDate.parse("11/11/2015", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd3.setStudio("Lighthouse");
        dvd3.setUserRating("3/10");

        dao.addDVD(dvd3.getDVDId(), dvd3);

        assertEquals(1, dao.getDVDsReleasedAfterNYears(5).size());
    }

    @Test
    public void testFindAllMoviesByMPAARating() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/1950", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/1960", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD dvd3 = new DVD("0003");
        dvd3.setDVDTitle("Fuzz");
        dvd3.setDirector("Bear");
        dvd3.setMPAARating("PG-13");
        dvd3.setReleaseDate(LocalDate.parse("11/11/2015", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd3.setStudio("Lighthouse");
        dvd3.setUserRating("3/10");

        dao.addDVD(dvd3.getDVDId(), dvd3);

        assertEquals(1, dao.getDVDsWithMPAARating("PG").size());
    }

    @Test
    public void testFindAllMoviesByDirector() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/1950", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/1960", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD dvd3 = new DVD("0003");
        dvd3.setDVDTitle("Fuzz");
        dvd3.setDirector("Bear");
        dvd3.setMPAARating("PG-13");
        dvd3.setReleaseDate(LocalDate.parse("11/11/2015", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd3.setStudio("Lighthouse");
        dvd3.setUserRating("3/10");

        dao.addDVD(dvd3.getDVDId(), dvd3);

        assertEquals(2, dao.getDVDsByDirector("Burton").size());
    }

    @Test
    public void testFindAllMoviesByStudio() throws Exception {
        DVD dvd1 = new DVD("0001");
        dvd1.setDVDTitle("Big Fish");
        dvd1.setDirector("Burton");
        dvd1.setMPAARating("PG-13");
        dvd1.setReleaseDate(LocalDate.parse("02/01/1950", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd1.setStudio("Paramount");
        dvd1.setUserRating("10/10");

        dao.addDVD(dvd1.getDVDId(), dvd1);

        DVD dvd2 = new DVD("0002");
        dvd2.setDVDTitle("Horse");
        dvd2.setDirector("Burton");
        dvd2.setMPAARating("PG");
        dvd2.setReleaseDate(LocalDate.parse("11/11/1960", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd2.setStudio("Miramax");
        dvd2.setUserRating("5/10");

        dao.addDVD(dvd2.getDVDId(), dvd2);

        DVD dvd3 = new DVD("0003");
        dvd3.setDVDTitle("Fuzz");
        dvd3.setDirector("Bear");
        dvd3.setMPAARating("PG-13");
        dvd3.setReleaseDate(LocalDate.parse("11/11/2015", DateTimeFormatter.ofPattern("MM/dd/yyyy")));
        dvd3.setStudio("Lighthouse");
        dvd3.setUserRating("3/10");

        dao.addDVD(dvd3.getDVDId(), dvd3);

        assertEquals(1, dao.getDVDsByStudio("Lighthouse").size());
    }

@Test
        public void testFindAverageAge() throws Exception {

    }

    @Test
        public void testFindNewestDVD() throws Exception {

    }

    @Test
        public void testFindOldestDVD() throws Exception {

    }

    @Test
        public void findAverageNumberOfNotes() throws Exception {

    }

}
