package com.sg.ayesberger.dvdlibrary.controller;

import com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDao;
import com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException;
import com.sg.ayesberger.dvdlibrary.dto.DVD;
import com.sg.ayesberger.dvdlibrary.ui.DVDLibraryView;
import java.util.List;
import java.util.Map;

public class DVDLibraryController {

    DVDLibraryView view;
    DVDLibraryDao dao;

    public DVDLibraryController(DVDLibraryDao dao, DVDLibraryView view) {
        this.dao = dao;
        this.view = view;
    }

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;

        try {
            while (keepGoing) {

                menuSelection = getMenuSelection();

                switch (menuSelection) {
                    case 1:
                        listDVDs();
                        break;
                    case 2:
                        createDVD();
                        break;
                    case 3:
                        viewDVD();
                        break;
                    case 4:
                        removeDVD();
                        break;
                    case 5:
                        displayNumberOfDVDs();
                        break;
                    case 6:
                        editDVD();
                        break;
                    case 7:
                        searchDVDs();
                        break;
                    case 8:
                        findDVDsInLastNYears();
                        break;
                    case 9:
                        findDVDsByMPAARating();
                        break;
                    case 10:
                        findDVDsByDirector();
                        break;
                    case 11:
                        findDVDsByStudio();
                        break;
                    case 12:
                        findAverageAgeOfDVDs();
                        break;
                    case 13:
                        findNewestDVD();
                        break;
                    case 14:
                        findOldestDVD();
                        break;
                    case 15:
                        findAverageNumberOfNotes();
                        break;
                    case 16:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                }
            }
            exitMessage();
        } catch (DVDLibraryDaoException e) {
            view.displayErrorMessage(e.getMessage());
        }
    }

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    private void createDVD() throws DVDLibraryDaoException {
        view.displayCreateDVDBanner();
        DVD newDVD = view.getNewDVDInfo();
        dao.addDVD(newDVD.getDVDId(), newDVD);
        view.displayCreateSuccessBanner();
    }

    private void listDVDs() throws DVDLibraryDaoException {
        List<DVD> DVDList = dao.getAllDVDs();
        view.displayDisplayAllBanner();
        view.displayDVDList(DVDList);
    }

    private void viewDVD() throws DVDLibraryDaoException {
        view.displayDisplayDVDBanner();
        String DVDId = view.getDVDIdChoice();
        DVD dvd = dao.getDVD(DVDId);
        view.displayDVD(dvd);
    }

    private void removeDVD() throws DVDLibraryDaoException {
        view.displayRemoveDVDBanner();
        String DVDId = view.getDVDIdChoice();
        dao.removeDVD(DVDId);
        view.displayRemoveSuccessBanner();
    }

    private void displayNumberOfDVDs() throws DVDLibraryDaoException {
        view.displayDisplayNumberOfDVDs();
        int numberOfDVDs = dao.numberOfDVDs();
        view.displayNumberOfDVDs(numberOfDVDs);
    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }

    private void exitMessage() {
        view.displayExitBanner();
    }

    private void searchDVDs() throws DVDLibraryDaoException {
        view.displaySearchBanner();
        String userTitle = view.getDVDTitle();
        DVD DVDTitle = dao.searchDVDs(userTitle);
        view.displayFoundDVD(DVDTitle);
    }

    private void editDVD() throws DVDLibraryDaoException {
        view.displayEditBanner();
        String DVDId = view.getDVDIdChoice();
        view.displayEditChoice(DVDId);
        DVD userChoice = dao.getDVD(DVDId);
        view.displayDVD(userChoice);
        view.getEditedDVDInfo(userChoice);
        DVD editedDVD = dao.editDVD(DVDId, userChoice);
        view.displayDVD(editedDVD);
        view.displayEditSuccess();
    }

    private void findDVDsInLastNYears() throws DVDLibraryDaoException {
        view.displayDisplayFindDVDsInLastNYearsBanner();
        int nYears = view.userChoiceYears();
        List<DVD> DVDList = dao.getDVDsReleasedAfterNYears(nYears);
        view.displayDVDList(DVDList);
    }

    private void findDVDsByMPAARating() throws DVDLibraryDaoException {
        view.displayDisplayGetDVDsByMPAABanner();
        String MPAARating = view.getUserMPAARatingChoice();
        List<DVD> DVDList = dao.getDVDsWithMPAARating(MPAARating);
        view.displayDVDList(DVDList);
    }

    private void findDVDsByDirector() throws DVDLibraryDaoException {
        view.displayDisplayGetDVDsByDirectorBanner();
        String director = view.getUserDirectorChoice();
        Map<String, List<DVD>> dvds = dao.getDVDsByDirector(director);
        view.displayDVDsByDirectorOrganizedByMPAA(dvds);
    }

    private void findDVDsByStudio() throws DVDLibraryDaoException {
        view.displayDisplayGetDVDsByStudio();
        String studio = view.getUserStudioChoice();
        List<DVD> DVDList = dao.getDVDsWithMPAARating(studio);
        view.displayDVDList(DVDList);
    }

    private void findAverageAgeOfDVDs() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void findNewestDVD() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void findOldestDVD() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void findAverageNumberOfNotes() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
