package com.sg.ayesberger.dvdlibrary.ui;

import com.sg.ayesberger.dvdlibrary.dto.DVD;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DVDLibraryView {

    private UserIO io;

    public DVDLibraryView(UserIO io) {
        this.io = io;
    }

    public int printMenuAndGetSelection() {
        io.print("Main Menu");
        io.print("1. List DVDs");
        io.print("2. Create New DVD");
        io.print("3. View a DVD");
        io.print("4. Remove a DVD");
        io.print("5. List Number of DVDs");
        io.print("6. Edit a DVD");
        io.print("7. Search for a DVD");
        io.print("8. Find DVDs released in the last x years");
        io.print("9. Find DVDs by MPAA Rating");
        io.print("10. Find movies by Director");
        io.print("11. Find DVDs by Studio");
        io.print("12. Display the average age of DVDs in the library");
        io.print("13. Display the newest DVD");
        io.print("14. Display oldest DVD");
        io.print("15. Display the average number of notes for DVDs");
        io.print("16. Exit");

        return io.readInt("Please select from the above choices.", 1, 15);
    }

    public DVD getNewDVDInfo() {
        String DVDId = io.readString("Please enter DVD Id");
        String DVDTitle = io.readString("Please enter the DVD title");
        LocalDate releaseDate = io.readLocalDate("Please enter the release date (mm/dd/yyyy)", false);
        String MPAARating = io.readString("Please enter the MPAA rating");
        String director = io.readString("Please enter the director");
        String studio = io.readString("Please enter the studio");
        String userRating = io.readString("Please enter your personal rating or notes");

        DVD currentDVD = new DVD(DVDId);

        currentDVD.setDVDTitle(DVDTitle);
        currentDVD.setDirector(director);
        currentDVD.setReleaseDate(releaseDate);
        currentDVD.setMPAARating(MPAARating);
        currentDVD.setStudio(studio);
        currentDVD.setUserRating(userRating);
        return currentDVD;
    }

    public void displayCreateDVDBanner() {
        io.print("=== Create DVD ===");
    }

    public void displayCreateSuccessBanner() {
        io.readString("DVD successfully created. Please hit enter to continue.");
    }

    public void displayDVDList(List<DVD> DVDList) {
        for (DVD currentDVD : DVDList) {
            io.print(currentDVD.getDVDId() + ": "
                    + currentDVD.getDVDTitle() + ": "
                    + currentDVD.getDirector() + ": "
                    + currentDVD.getReleaseDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")) + ": "
                    + currentDVD.getMPAARating() + ": "
                    + currentDVD.getStudio() + ": "
                    + currentDVD.getUserRating());
        }
        io.readString("Please hit enter to continue.");
    }

    public void displayDisplayAllBanner() {
        io.print("=== Display All DVDs ===");
    }

    public String getDVDIdChoice() {
        return io.readString("Please enter the DVD ID.");
    }

    public void displayDVD(DVD dvd) {
        if (dvd != null) {
            io.print(dvd.getDVDId());
            io.print(dvd.getDVDTitle());
            io.print(dvd.getDirector());
            io.print(dvd.getReleaseDate().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
            io.print(dvd.getMPAARating());
            io.print(dvd.getStudio());
            io.print(dvd.getUserRating());
            io.print("");
        } else {
            io.print("No such DVD.");
        }
        io.readString("Please hit enter to continue.");
    }

    public void displayDisplayDVDBanner() {
        io.print("=== Display DVD ===");
    }

    public void displayRemoveDVDBanner() {
        io.print("=== Remove DVD ===");
    }

    public void displayRemoveSuccessBanner() {
        io.print("DVD successfully removed. Please hit enter to continue.");
    }

    public void displayDisplayNumberOfDVDs() {
        io.print("=== Number of DVDs ===");
    }

    public void displayNumberOfDVDs(int numberOfDVDs) {
        io.print("You have " + numberOfDVDs + " DVDs.");
    }

    public void displayExitBanner() {
        io.print("Good Bye!");
    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("=== ERROR ===");
        io.print(errorMsg);
    }

    public void displaySearchBanner() {
        io.print("=== Search for a DVD ===");
    }

    public String getDVDTitle() {
        return io.readString("Please enter the Title of the DVD you want to search for.");
    }

    public void displayFoundDVD(DVD foundDVD) {
        io.print("You have: ");
        if (foundDVD != null) {
            io.print(foundDVD.getDVDId());
            io.print(foundDVD.getDVDTitle());
            io.print(foundDVD.getDirector());
            io.print(foundDVD.getReleaseDate().toString());
            io.print(foundDVD.getMPAARating());
            io.print(foundDVD.getStudio());
            io.print(foundDVD.getUserRating());
            io.print("");
        } else {
            io.print("No such DVD found.");
        }
        io.readString("Please hit enter to continue.");
    }

    public void displayEditBanner() {
        io.print("=== Edit DVD ===");
    }

    public void displayEditSuccess() {
        io.print("DVD successfully edited.");
    }

    public void displayEditChoice(String DVDId) {
        io.print("You are editing: " + DVDId);
    }

    public void getEditedDVDInfo(DVD DVDToEdit) {
        String DVDTitle = io.readString("Please enter the edited DVD title or hit enter to continue");
        LocalDate releaseDate = io.readLocalDate("Please enter the edited year released or hit enter to continue", true);
        String MPAARating = io.readString("Please enter the edited MPAA rating or hit enter to continue");
        String director = io.readString("Please enter the edited director or hit enter to continue");
        String studio = io.readString("Please enter the edited studio or hit enter to continue");
        String userRating = io.readString("Please enter your edited personal rating or notes, or hit enter to continue");

        if (!DVDTitle.isEmpty()) {
            DVDToEdit.setDVDTitle(DVDTitle);
        }
        if (!director.isEmpty()) {
            DVDToEdit.setDirector(director);
        }
        if (releaseDate == null) {
            DVDToEdit.setReleaseDate(releaseDate);
        }
        if (!MPAARating.isEmpty()) {
            DVDToEdit.setMPAARating(MPAARating);
        }
        if (!studio.isEmpty()) {
            DVDToEdit.setStudio(studio);
        }
        if (!userRating.isEmpty()) {
            DVDToEdit.setUserRating(userRating);
        }
    }

    public void displayDisplayFindDVDsInLastNYearsBanner() {
        io.print("=== Show DVDs Released in the Past N Years ===");
    }

    public int userChoiceYears() {
        return io.readInt("Please enter how many years back to find DVDs: ");
    }

    public void displayDisplayGetDVDsByMPAABanner() {
        io.print("=== Show DVDs by MPAA Rating ===");
    }

    public String getUserMPAARatingChoice() {
        return io.readString("Please enter the MPAA Rating you would like to search for: ");
    }

    public void displayDisplayGetDVDsByDirectorBanner() {
        io.print("=== Show DVDs by Director ===");
    }

    public String getUserDirectorChoice() {
        return io.readString("Please enter the name of the Director you would like to search for: ");
    }

    public void displayDVDsByDirectorOrganizedByMPAA(Map<String, List<DVD>> dvds) {
        Set<String> directors = dvds.keySet();
        directors.stream()
                .forEach(rating -> {
                    io.print("=======================================");
                    io.print("MPAA Rating: " + rating);
                    dvds.get(rating).stream().forEach(s -> io.print(s.getDVDTitle()));
                });
        io.readString("Please hit enter to continue.");
    }

    public void displayDisplayGetDVDsByStudio() {
        io.print("=== Show DVDs by Studio ===");
    }

    public String getUserStudioChoice() {
        return io.readString("Please enter the Studio you would like to search for: ");
    }
}
