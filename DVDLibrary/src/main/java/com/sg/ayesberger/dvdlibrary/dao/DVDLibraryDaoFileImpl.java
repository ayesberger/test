package com.sg.ayesberger.dvdlibrary.dao;

import com.sg.ayesberger.dvdlibrary.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class DVDLibraryDaoFileImpl implements DVDLibraryDao {

    public static final String LIBRARY_FILE = "library.txt";
    public static final String DELIMITER = "::";

    private Map<String, DVD> dvds = new HashMap<>();

    @Override
    public DVD addDVD(String DVDId, DVD dvd) throws DVDLibraryDaoException {
        DVD newDVD = dvds.put(DVDId, dvd);
        writeLibrary();
        return newDVD;
    }

    @Override
    public List<DVD> getAllDVDs() throws DVDLibraryDaoException {
        loadLibrary();
        return new ArrayList<DVD>(dvds.values());
    }

    @Override
    public DVD getDVD(String DVDId) throws DVDLibraryDaoException {
        loadLibrary();
        return dvds.get(DVDId);
    }

    @Override
    public DVD removeDVD(String DVDId) throws DVDLibraryDaoException {
        DVD removedDVD = dvds.remove(DVDId);
        writeLibrary();
        return removedDVD;
    }

    @Override
    public int numberOfDVDs() throws DVDLibraryDaoException {
        int numberOfDVDs = dvds.size();
        return numberOfDVDs;
    }

    @Override
    public DVD searchDVDs(String DVDTitle) throws DVDLibraryDaoException {

        DVD foundDVD = null;

        for (Map.Entry<String, DVD> entry : dvds.entrySet()) {

            foundDVD = entry.getValue();

            if (foundDVD.getDVDTitle().equals(DVDTitle)) {
                return foundDVD;
            } else {
                foundDVD = null;
            }
        }
        return foundDVD;
    }

    @Override
    public DVD editDVD(String DVDId, DVD userChoice) throws DVDLibraryDaoException {
        DVD editedDVD = dvds.put(DVDId, userChoice);
        writeLibrary();
        return editedDVD;
    }

    private void loadLibrary() throws DVDLibraryDaoException {

        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(LIBRARY_FILE)))) {

            String currentLine;
            String[] currentTokens;

            while (scanner.hasNextLine()) {
                currentLine = scanner.nextLine();
                currentTokens = currentLine.split(DELIMITER);
                DVD currentDVD = new DVD(currentTokens[0]);
                currentDVD.setDVDTitle(currentTokens[1]);
                currentDVD.setDirector(currentTokens[2]);
                currentDVD.setReleaseDate(LocalDate.parse(currentTokens[3]));
                currentDVD.setMPAARating(currentTokens[4]);
                currentDVD.setStudio(currentTokens[5]);
                currentDVD.setUserRating(currentTokens[6]);

                dvds.put(currentDVD.getDVDId(), currentDVD);
            }

        } catch (FileNotFoundException e) {
            throw new DVDLibraryDaoException("-_- Could not load library data into memory.", e);
        }
    }

    private void writeLibrary() throws DVDLibraryDaoException {
        try (PrintWriter out = new PrintWriter(new FileWriter(LIBRARY_FILE))) {
            List<DVD> DVDList = this.getAllDVDs();
            for (DVD currentDVD : DVDList) {
                out.println(currentDVD.getDVDId() + DELIMITER
                        + currentDVD.getDVDTitle() + DELIMITER
                        + currentDVD.getDirector() + DELIMITER
                        + currentDVD.getReleaseDate() + DELIMITER
                        + currentDVD.getMPAARating() + DELIMITER
                        + currentDVD.getStudio() + DELIMITER
                        + currentDVD.getUserRating());

                out.flush();
            }

        } catch (IOException e) {
            throw new DVDLibraryDaoException("Could not save library data.", e);
        }
    }

    @Override
    public List<DVD> getDVDsReleasedAfterNYears(int userYear) throws DVDLibraryDaoException {
        LocalDate userYearLocalDate = LocalDate.now().minusYears(userYear);
        return dvds.values()
                .stream()
                .filter(s -> s.getReleaseDate().isAfter(userYearLocalDate))
                .collect(Collectors.toList());
    }

    @Override
    public List<DVD> getDVDsWithMPAARating(String MPAARating) throws DVDLibraryDaoException {
        return dvds.values()
                .stream()
                .filter(s -> s.getMPAARating().equalsIgnoreCase(MPAARating))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, List<DVD>> getDVDsByDirector(String director) throws DVDLibraryDaoException {
        return dvds.values()
                .stream()
                .filter(s -> s.getDirector().equalsIgnoreCase(director))
                .collect(Collectors.groupingBy(s -> s.getMPAARating()));
    }

    @Override
    public List<DVD> getDVDsByStudio(String studio) throws DVDLibraryDaoException {
        return dvds.values()
                .stream()
                .filter(s -> s.getStudio().equalsIgnoreCase(studio))
                .collect(Collectors.toList());
    }

    @Override
    public int averageAgeOfMovies() throws DVDLibraryDaoException {
        BigInteger total = BigInteger.ZERO;
        for (String key : dvds.keySet()) {
            total = total.add(BigInteger.valueOf(DVD.getReleaseDate()))
            
        }
    }

    @Override
    public DVD newestMovie() throws DVDLibraryDaoException {
        dvds = Map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));

        /*
        public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
    return map.entrySet()
              .stream()
              .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()))
              .collect(Collectors.toMap(
                Map.Entry::getKey, 
                Map.Entry::getValue, 
                (e1, e2) -> e1, 
                LinkedHashMap::new
              ));
}
         */
        //return dvds.values()
        //      .stream()
        //    .collect(Collections.max( ??? ));
    }

    @Override
    public DVD oldestMovie() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int averageNumberOfNotes() throws DVDLibraryDaoException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
