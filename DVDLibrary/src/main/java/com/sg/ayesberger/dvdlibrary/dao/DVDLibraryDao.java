package com.sg.ayesberger.dvdlibrary.dao;

import com.sg.ayesberger.dvdlibrary.dto.DVD;
import java.util.List;
import java.util.Map;

public interface DVDLibraryDao {

    /**
     * Adds the given DVD to the library and associates it with the given DVD
     * id. If there is already a DVD associated with the given DVD id it will
     * return that DVD object, otherwise it will return null.
     *
     * @param DVDId id with which DVD is to be associated
     * @param dvd DVD to be added to the library return the DVD object
     * previously associated with the given DVD id if it exists, null otherwise
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD addDVD(String DVDId, DVD dvd) throws DVDLibraryDaoException;

    /**
     * Returns a String array containing the DVD ids of all the DVDs in the
     * library.
     *
     * @return String array containing the ids of all the DVDs in the library
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    List<DVD> getAllDVDs() throws DVDLibraryDaoException;

    /**
     * Returns the DVD object associated with the given id. Returns null if no
     * such DVD exists.
     *
     * @paramDVDId ID of the DVD to retrieve
     * @return the DVD object associated with the given DVD id, null if no such
     * DVD exists
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD getDVD(String DVDId) throws DVDLibraryDaoException;

    /**
     * Removes from the library the DVD associated with the given id. Returns
     * the DVD object that is being removed or null if no DVD was associated
     * with the given DVD id.
     *
     * @paramDVDId ID of the DVD to retrieve
     * @return the DVD object associated with the given DVD id, null if no such
     * DVD exists
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD removeDVD(String DVDId) throws DVDLibraryDaoException;

    /**
     * Displays number of DVDs in the library.
     *
     * @return Number of contacts in the address book.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    int numberOfDVDs() throws DVDLibraryDaoException;

    /**
     * Searches the library for a DVD by title. Returns the DVD object
     * associated with the given title. Returns null if no such DVD exists.
     *
     * @param DVDTitle Title of the DVD to search for
     * @return the DVD object associated with the given title, null if no such
     * DVD exists
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD searchDVDs(String DVDTitle) throws DVDLibraryDaoException;

    /**
     * Edits the values of the DVD object.
     *
     * @param userChoice the DVD the user wants to edit.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD editDVD(String DVDId, DVD userChoice) throws DVDLibraryDaoException;

    /**
     * Returns a list of DVDs released after the userYear. If no such DVDs exist
     * returns null instead.
     * 
     * @param userYear the year the user enters to get the DVDs listed after
     * @return list of DVDs released after the userYear. If no such DVDs exist returns null instead.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    List <DVD> getDVDsReleasedAfterNYears(int userYear) throws DVDLibraryDaoException;
    
    /**
     * Returns a list of DVDs with a given MPAA Rating. Returns null if no such DVD exists.
     * 
     * @param MPAARating the MPAA Rating of the DVDs to match.
     * @return list of DVDs with the given MPAA Rating.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    List <DVD> getDVDsWithMPAARating(String MPAARating) throws DVDLibraryDaoException;
    
    /**
     * Returns a list of DVDs with a given director. Returns null if no such DVD exists.
     * 
     * @param director the director of the DVDs to match. 
     * @return list of DVDs with a given director.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException 
     */
    Map<String, List <DVD>> getDVDsByDirector(String director) throws DVDLibraryDaoException;
    
    /**
     * Returns a list of DVDs with a given studio. Returns null if no such DVD exists.
     * 
     * @param studio the director of the DVDs to match.
     * @return list of DVDs with a given studio.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    List <DVD> getDVDsByStudio(String studio) throws DVDLibraryDaoException;
    
    /**
     * Returns the average age of the movies in the DVD Library.
     * 
     * @return the average age of the movies in the library.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    int averageAgeOfMovies() throws DVDLibraryDaoException;
    
    /**
     * Returns the most recent DVD in the DVD Library.
     * @return the most recent DVD in the library.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException
     */
    DVD newestMovie() throws DVDLibraryDaoException;
    
    /**
     * Returns the oldest DVD in the DVD Library.
     * 
     * @return the most recent DVD in the library.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException 
     */
    DVD oldestMovie() throws DVDLibraryDaoException;
    
    /**
     * Returns the average number of notes for DVDs in the DVD library.
     * 
     * @return the average number of notes for DVDs in the DVD library.
     * @throws com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoException 
     */
    int averageNumberOfNotes() throws DVDLibraryDaoException;

}
