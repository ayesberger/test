package com.sg.ayesberger.dvdlibrary.ui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class UserIOConsoleImpl implements UserIO {

    Scanner userInput = new Scanner(System.in);

    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public double readDouble(String prompt) {
        System.out.println(prompt);
        String stringUserDouble = userInput.nextLine();
        Double userDouble = Double.parseDouble(stringUserDouble);

        return userDouble;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double userDouble = 0;
        boolean validNumber = false;
        while (!validNumber) {
            System.out.println(prompt);
            String stringUserDouble = userInput.nextLine();
            userDouble = Double.parseDouble(stringUserDouble);

            if (userDouble >= min && userDouble <= max) {
                validNumber = true;

            } else {
                System.out.println("Please enter a valid number.");
            }

        }
        return userDouble;
    }

    @Override
    public float readFloat(String prompt) {
        System.out.println(prompt);
        String stringUserFloat = userInput.nextLine();
        float userFloat = Float.parseFloat(stringUserFloat);

        return userFloat;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float userFloat = 0;
        boolean validNumber = false;
        while (!validNumber) {
            System.out.println(prompt);
            String stringUserFloat = userInput.nextLine();
            userFloat = Float.parseFloat(stringUserFloat);

            if (userFloat >= min && userFloat <= max) {
                validNumber = true;

            } else {
                System.out.println("Please enter a valid number.");
            }

        }
        return userFloat;
    }

    @Override
    public int readInt(String prompt) {
        System.out.println(prompt);
        String stringUserInt = userInput.nextLine();
        int userInt = Integer.parseInt(stringUserInt);

        return userInt;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int userInt = 0;
        boolean validNumber = false;
        while (!validNumber) {
            System.out.println(prompt);
            String stringUserInt = userInput.nextLine();
            userInt = Integer.parseInt(stringUserInt);

            if (userInt >= min && userInt <= max) {
                validNumber = true;

            } else {
                System.out.println("Please enter a valid number.");
            }

        }
        return userInt;
    }

    @Override
    public long readLong(String prompt) {
        System.out.println(prompt);
        String stringUserLong = userInput.nextLine();
        long userLong = Long.parseLong(stringUserLong);

        return userLong;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long userLong = 0;
        boolean validNumber = false;
        while (!validNumber) {
            System.out.println(prompt);
            String stringUserLong = userInput.nextLine();
            userLong = Long.parseLong(stringUserLong);

            if (userLong >= min && userLong <= max) {
                validNumber = true;

            } else {
                System.out.println("Please enter a valid number.");
            }

        }
        return userLong;
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        String stringUserInput = userInput.nextLine();

        return stringUserInput;
    }

    @Override
    public LocalDate readLocalDate(String prompt, boolean returnNull) {

        boolean validDate = false;
        LocalDate date = LocalDate.now();
        while (!validDate) {
            System.out.println(prompt);
            String stringUserLocalDate = userInput.nextLine();
            DateTimeFormatter formater = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            if (stringUserLocalDate.isEmpty() && returnNull) {
                return null;
            } else {
                try {
                    date = LocalDate.parse(stringUserLocalDate, formater);
                    validDate = true;

                } catch (Exception e) {
                    System.out.println("ERROR: Please enter a valid date. (mm/dd/yyyy)");
                }
            }
        }
        return date;
    }
}
