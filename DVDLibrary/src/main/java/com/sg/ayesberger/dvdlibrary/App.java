package com.sg.ayesberger.dvdlibrary;

import com.sg.ayesberger.dvdlibrary.controller.DVDLibraryController;
import com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDao;
import com.sg.ayesberger.dvdlibrary.dao.DVDLibraryDaoFileImpl;
import com.sg.ayesberger.dvdlibrary.ui.DVDLibraryView;
import com.sg.ayesberger.dvdlibrary.ui.UserIO;
import com.sg.ayesberger.dvdlibrary.ui.UserIOConsoleImpl;

public class App {

    public static void main(String[] args) {
        UserIO myIO = new UserIOConsoleImpl();
        DVDLibraryView myView = new DVDLibraryView(myIO);
        DVDLibraryDao myDao = new DVDLibraryDaoFileImpl();
        DVDLibraryController controller = new DVDLibraryController(myDao, myView);
        controller.run();
    }
}
