package com.sg.ayesberger.dvdlibrary.dto;

import java.time.LocalDate;
import java.util.Objects;

public class DVD {

    private String DVDTitle;
    private String director;
    private LocalDate releaseDate;
    private String DVDId;
    private String MPAARating;
    private String studio;
    private String userRating;

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getMPAARating() {
        return MPAARating;
    }

    public void setMPAARating(String MPAARating) {
        this.MPAARating = MPAARating;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    public String getUserRating() {
        return userRating;
    }

    public void setUserRating(String userRating) {
        this.userRating = userRating;
    }

    public DVD(String DVDId) {
        this.DVDId = DVDId;
    }

    public String getDVDTitle() {
        return DVDTitle;
    }

    public void setDVDTitle(String DVDTitle) {
        this.DVDTitle = DVDTitle;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDVDId() {
        return DVDId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.DVDTitle);
        hash = 83 * hash + Objects.hashCode(this.director);
        hash = 83 * hash + Objects.hashCode(this.releaseDate);
        hash = 83 * hash + Objects.hashCode(this.DVDId);
        hash = 83 * hash + Objects.hashCode(this.MPAARating);
        hash = 83 * hash + Objects.hashCode(this.studio);
        hash = 83 * hash + Objects.hashCode(this.userRating);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DVD other = (DVD) obj;
        if (!Objects.equals(this.DVDTitle, other.DVDTitle)) {
            return false;
        }
        if (!Objects.equals(this.director, other.director)) {
            return false;
        }
        if (!Objects.equals(this.releaseDate, other.releaseDate)) {
            return false;
        }
        if (!Objects.equals(this.DVDId, other.DVDId)) {
            return false;
        }
        if (!Objects.equals(this.MPAARating, other.MPAARating)) {
            return false;
        }
        if (!Objects.equals(this.studio, other.studio)) {
            return false;
        }
        if (!Objects.equals(this.userRating, other.userRating)) {
            return false;
        }
        return true;
    }

}
