package com.sg.ayesberger.userioclass;

public class SimpleCalculator {

    private int result;
    private int operandA;
    private int operandB;

    public SimpleCalculator() {
        this.operandA = operandA;
        this.operandB = operandB;
    }

    public int add(int operandA, int operandB) {
        result = operandA + operandB;
        return result;
    }

    public int subtract(int operandA, int operandB) {
        result = operandA - operandB;
        return result;
    }

    public int multiply(int operandA, int operandB) {
        result = operandA * operandB;
        return result;
    }

    public int divide(int operandA, int operandB) {
        result = operandA / operandB;
        return result;
    }

}
