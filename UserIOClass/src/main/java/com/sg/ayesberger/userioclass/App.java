package com.sg.ayesberger.userioclass;

public class App {

    private static int userOperationChoice = 0;
    private static int operandA;
    private static int operandB;
    private static int result;
    private static String nameOfOperation;
    private static final UserInputOutput USERINPUTOUTPUT = new UserInputOutput();

    public static int getUserOperationChoice() {
        return userOperationChoice;
    }

    public static int getOperandA() {
        return operandA;
    }

    public static int getOperandB() {
        return operandB;
    }

    public static int getResult() {
        return result;
    }

    public static void main(String[] args) {

        USERINPUTOUTPUT.print("Calculator Time!");
        boolean keepCalculating = true;
        do {
            promptForOperationType();

            exitProgram();

            promptForOperands();

            printResultOfCalculation(); 

        } while (keepCalculating = true);
    }

    private static void printResultOfCalculation() {
        SimpleCalculator calculator = new SimpleCalculator();
        if (userOperationChoice == 1) {
            result = calculator.add(operandA, operandB);
        }
        if (userOperationChoice == 2) {
            result = calculator.subtract(operandA, operandB);
        }
        if (userOperationChoice == 3) {
            result = calculator.multiply(operandA, operandB);
        }
        if (userOperationChoice == 4) {
            result = calculator.divide(operandA, operandB);
        }
        USERINPUTOUTPUT.print("The answer is " + result);
    }

    private static void exitProgram() {
        if (userOperationChoice == 5) {
            USERINPUTOUTPUT.print("Thank you for using this calculator. Have a great day!");
            System.exit(0);
        } else {
            
        }
    }

    private static void promptForOperands() {
        
        USERINPUTOUTPUT.print("What numbers would you like to " + namesOperation() + "?");
        operandA = USERINPUTOUTPUT.readInt("First number: ");
        operandB = USERINPUTOUTPUT.readInt("Second number: ");
     
    }

    private static int promptForOperationType() {
        
        userOperationChoice = USERINPUTOUTPUT.readInt("Would you like to 1) Add 2) Subtract 3) Multiply 4) Divide 5) Quit ? ", 1, 5);
        return userOperationChoice;
    }

    private static String namesOperation() {
        String nameOfOperation = null;
        switch (userOperationChoice) {
            case 1:
                nameOfOperation = "Add ";
                break;
            case 2:
                nameOfOperation = "Subtract ";
                break;
            case 3:
                nameOfOperation = "Multiply ";
                break;
            case 4:
                nameOfOperation = "Divide ";
                break;
        }
        return nameOfOperation;
    }

}
